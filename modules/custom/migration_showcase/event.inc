<?php

class EventMigration extends Migration {
  public function __construct() {
    parent::__construct();
    $query = Database::getConnection('default', 'migrate')
        ->select('event_data', 'ed')
        ->fields('ed', array('id', 'name', 'date', 'tz', 'venue_text', 'venue_url', 'description'))
    ;
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationNode('event');
    $sourceKey = array(
      'id' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ),
    );
    $destKey = MigrateDestinationNode::getKeySchema();
    $this->map = new MigrateSQLMap($this->machineName, $sourceKey, $destKey);
    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('uid')->defaultValue(1)->description('Make admin the author of migrated content');
    $this->addFieldMapping('created')->description('Use current timestamp');
    $this->addFieldMapping('field_date', 'date');
    $this->addFieldMapping('field_date:timezone', 'tz');
    $this->addFieldMapping('field_venue', 'venue_url');
    $this->addFieldMapping('field_venue:title', 'venue_text');
    $this->addFieldMapping('body', 'description');
    $this->addFieldMapping('body:format')->defaultValue('plain_text');
  }

  public function prepare($node, stdClass $row) {
    if(!empty($row->tz)) {
      // The 'field_date:timezone' mapping does not seem to work for some unknown reason.
      // So we have to do this.
      $node->field_date[LANGUAGE_NONE][0]['timezone'] = $row->tz;
    }
  }
}