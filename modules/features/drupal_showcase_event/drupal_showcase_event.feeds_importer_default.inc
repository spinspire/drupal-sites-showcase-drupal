<?php
/**
 * @file
 * drupal_showcase_event.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function drupal_showcase_event_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'events_csv';
  $feeds_importer->config = array(
    'name' => 'Events CSV',
    'description' => 'A feeds importer for creating Event nodes from a CSV file',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'direct' => 1,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '0',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'ID',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Name',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Date',
            'target' => 'field_date:start',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Venue Text',
            'target' => 'field_venue:title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Venue URL',
            'target' => 'field_venue:url',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Description',
            'target' => 'body',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'event',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['events_csv'] = $feeds_importer;

  return $export;
}
